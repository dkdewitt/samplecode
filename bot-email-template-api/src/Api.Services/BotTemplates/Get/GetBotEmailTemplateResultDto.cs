﻿using System;
using System.Text.Json.Serialization;
using Common;
using Api.Services.Converters;
namespace Api.Services.BotTemplates
{
    public class GetBotEmailTemplateResultDto
    {
        public Guid Id { get; set; }
        public Guid BotId { get; set; }
        public string Body { get; set; }
        public string Subject { get; set; }
        public MessageType MessageType { get; set; }
        [JsonConverter(typeof(MetadataConverter))]
        public IMessageMetadata Metadata { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }

    }
}
 
