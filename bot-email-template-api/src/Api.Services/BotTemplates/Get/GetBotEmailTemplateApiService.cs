﻿using System;
using Domain.Services.BotEmailTemplates;

namespace Api.Services.BotTemplates.Get
{
    public class GetBotEmailTemplateApiService : IGetBotEmailTemplateApiService
    {
        private readonly IGetBotEmailTemplateService getBotEmailTemplateService;
        public GetBotEmailTemplateApiService(IGetBotEmailTemplateService getBotEmailTemplateService)
        {
            this.getBotEmailTemplateService = getBotEmailTemplateService;
        }

        public async Task<IEnumerable<GetBotEmailTemplateResultDto>> GetBotEmailTemplatesByBotID(Guid botID)
        {
            var templates =  await this.getBotEmailTemplateService.GetBotEmailTemplatesByBotID(botID).ConfigureAwait(false);

            return templates.Select(t => new GetBotEmailTemplateResultDto
            {
                Id = t.Id,
                BotId = t.BotId,
                Body = t.Body,
                Subject = t.Subject,
                MessageType = t.MessageType,
                Metadata = t.Metadata,
                CreatedAt = t.CreatedAt,
                UpdatedAt = t.UpdatedAt
            });
        }
    }
}

