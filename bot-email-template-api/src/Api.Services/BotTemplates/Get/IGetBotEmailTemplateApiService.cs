﻿using System;
namespace Api.Services.BotTemplates.Get
{
    public interface IGetBotEmailTemplateApiService
    {
        Task<IEnumerable<GetBotEmailTemplateResultDto>> GetBotEmailTemplatesByBotID(Guid botID);
    }
}

