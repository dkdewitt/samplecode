﻿using System;
namespace Api.Services.BotTemplates.Create
{
    public interface ICreateBotEmailTemplateApiService
    {
        Task<CreateBotEmailTemplateResultDto> Create(CreateBotTemplateRequestDto request);
    }
}

