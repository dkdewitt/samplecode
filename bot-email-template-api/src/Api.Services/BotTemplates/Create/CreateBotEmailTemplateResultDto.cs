﻿using System;
using Common;
namespace Api.Services.BotTemplates
{
    public class CreateBotEmailTemplateResultDto
    {
        public Guid Id { get; set; }
        public Guid BotId { get; set; }
        public string Body { get; set; }
        public string Subject { get; set; }
        public MessageType MessageType { get; set; }
        public IMessageMetadata Metadata { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }

    }
}
 
