﻿using System;
using Domain.Services.BotTemplates;
using Common;

namespace Api.Services.BotTemplates.Create
{
    public class CreateBotTemplateRequestDto
    {
        public Guid BotId { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public MessageType MessageType { get; set; }
        public IMessageMetadata Metadata { get; set; }
    }
}

