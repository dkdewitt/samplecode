﻿using System;
using Domain.Services.BotTemplates;

namespace Api.Services.BotTemplates.Create
{
    public class CreateBotEmailTemplateApiService: ICreateBotEmailTemplateApiService
    {
        private readonly ICreateBotEmailTemplateService createBotEmailTemplateService;
        public CreateBotEmailTemplateApiService(ICreateBotEmailTemplateService createBotEmailTemplateService)
        {
            this.createBotEmailTemplateService = createBotEmailTemplateService;
        }

        public async Task<CreateBotEmailTemplateResultDto> Create(CreateBotTemplateRequestDto request)
        {
            var botEmailTemplate = new CreateBotEmailTemplate
            {
                Id = Guid.NewGuid(),
                BotId = request.BotId,
                Body = request.Body,
                Subject = request.Subject,
                MessageType = request.MessageType,
                Metadata = request.Metadata    

            };

            var newBotEmailTemplate = await this.createBotEmailTemplateService.CreateBotEmailTemplate(botEmailTemplate).ConfigureAwait(false);

            return new CreateBotEmailTemplateResultDto
            {
                Id = newBotEmailTemplate.Id,
                BotId = newBotEmailTemplate.BotId,
                Body = newBotEmailTemplate.Body,
                Subject = newBotEmailTemplate.Subject,
                Metadata = newBotEmailTemplate.Metadata,
                MessageType = newBotEmailTemplate.MessageType,
                CreatedAt = newBotEmailTemplate.CreatedAt,
                UpdatedAt = newBotEmailTemplate.UpdatedAt

            };
        }
    }
}

