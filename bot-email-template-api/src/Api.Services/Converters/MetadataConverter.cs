﻿using System;
using System.Text.Json;
using System.Text.Json.Serialization;
using Common;

namespace Api.Services.Converters
{
    public class MetadataConverter : JsonConverter<IMessageMetadata>
    {
        public override IMessageMetadata? Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            throw new NotImplementedException();
        }

        public override void Write(Utf8JsonWriter writer, IMessageMetadata value, JsonSerializerOptions options)
        {
            var x = value;
            Type type = value.GetType();
            if(x is Common.FirstService)
            {
                writer.WriteStringValue(JsonSerializer.Serialize<FirstService>((FirstService)x));
            }

        }
    }
}