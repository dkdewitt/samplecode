﻿using System;
using Data.DataAccess;

namespace Domain.Services.BotTemplates.Commands
{
    public class CreateBotTemplateQuery : IQuery
    {
        public string Sql => @"
            INSERT INTO public.bot_email_templates(
	            id,
                bot_id,
                subject,
                body,
                message_type,
                metadata,
                created_at,
                updated_at
            )
            VALUES (
                @Id,
                @BotId,
                @Body,
                @Subject,
                @MessageType,
                @Metadata::json,
                @CreatedAt,
                @UpdatedAt
);";

        public object Parameters => this;

        public Guid Id { get; set; }

        public Guid BotId  { get; set; }

        public string Body { get; set; }

        public string Subject { get; set; }

        public string MessageType { get; set; }

        public string Metadata { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime UpdatedAt { get; set; }

    }
}


