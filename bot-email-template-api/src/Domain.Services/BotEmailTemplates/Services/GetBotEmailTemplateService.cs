﻿using System;
using System.Text.Json;
using Common;
using Data.DataAccess;
using Domain.Services.BotTemplates;
using Domain.Services.BotTemplates.Queries;

namespace Domain.Services.BotEmailTemplates
{
    public class GetBotEmailTemplateService: IGetBotEmailTemplateService
    {
        public readonly IDataContext dataContext;
        public GetBotEmailTemplateService(IDataContext dataContext)
        {
            this.dataContext = dataContext;
        }

        public async Task<IEnumerable<BotEmailTemplate>> GetBotEmailTemplatesByBotID(Guid botID)
        {
            var result = await this.dataContext.QueryAsync<BotEmailTemplateQueryRequest>(new GetBotTemplatesByBotIDQuery { BotID = botID }).ConfigureAwait(false);

            return result.Select(r => new BotEmailTemplate
            {
                Id = r.Id,
                BotId = r.BotId,
                Body = r.Body,
                Subject = r.Subject,
                IsSystem = r.IsSystem,
                Metadata = JsonSerializer.Deserialize<FirstService>(r.Metadata),
                MessageType = Enum.Parse<MessageType>(r.MessageType),
                CreatedAt = r.CreatedAt,
                UpdatedAt = r.UpdatedAt
            });
        }
        
    }
}

