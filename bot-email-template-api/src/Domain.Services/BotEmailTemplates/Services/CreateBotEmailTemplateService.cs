﻿using System;
using System.Text.Json;
using Common;
using Data;
using Data.DataAccess;
using Domain.Services.BotTemplates.Commands;

namespace Domain.Services.BotTemplates
{
    public class CreateBotEmailTemplateService: ICreateBotEmailTemplateService
    {
        public readonly IDataContext dataContext;
        public CreateBotEmailTemplateService(IDataContext dataContext)
        {
            this.dataContext = dataContext;
        }

        public async Task<BotEmailTemplate> CreateBotEmailTemplate(CreateBotEmailTemplate createBotEmailTemplate)
        {
            var dt = DateTime.Now;
            var metadata = JsonSerializer.Serialize<FirstService>((FirstService)createBotEmailTemplate.Metadata);
            await this.dataContext.ExecuteNonQueryAsync<dynamic>(
                new CreateBotTemplateQuery
                {
                    Id = createBotEmailTemplate.Id,
                    BotId = createBotEmailTemplate.BotId,
                    Subject = createBotEmailTemplate.Subject,
                    Body = createBotEmailTemplate.Body,
                    MessageType = createBotEmailTemplate.MessageType.ToString(),
                    Metadata = metadata,
                    CreatedAt = dt,
                    UpdatedAt = dt
                }).ConfigureAwait(false);

            return new BotEmailTemplate
            {
                Id = createBotEmailTemplate.Id,
                BotId = createBotEmailTemplate.BotId,
                Subject = createBotEmailTemplate.Subject,
                Body = createBotEmailTemplate.Body,
                MessageType = createBotEmailTemplate.MessageType,
                Metadata = createBotEmailTemplate.Metadata,
                CreatedAt = dt,
                UpdatedAt = dt
            };
        }
    }
}

