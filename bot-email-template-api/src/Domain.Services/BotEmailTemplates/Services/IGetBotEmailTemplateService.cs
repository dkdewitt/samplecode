﻿using System;
using Domain.Services.BotTemplates;

namespace Domain.Services.BotEmailTemplates
{
    public interface IGetBotEmailTemplateService
    {
        Task<IEnumerable<BotEmailTemplate>> GetBotEmailTemplatesByBotID(Guid botID);
    }
}

