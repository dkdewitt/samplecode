﻿using System;
namespace Domain.Services.BotTemplates
{
    public interface ICreateBotEmailTemplateService
    {
        Task<BotEmailTemplate> CreateBotEmailTemplate(CreateBotEmailTemplate createBotEmailTemplate);
    }
}

