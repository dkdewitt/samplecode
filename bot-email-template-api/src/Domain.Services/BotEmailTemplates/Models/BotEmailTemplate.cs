﻿using System;
using Common;

namespace Domain.Services.BotTemplates
{
    public class BotEmailTemplate
    {
        public Guid Id { get; set; }
        public Guid BotId { get; set; }
        public string Body { get; set; }
        public string Subject { get; set; }
        public bool IsSystem { get; set; }
        public MessageType MessageType { get; set; }
        public IMessageMetadata Metadata { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }

    }
}
