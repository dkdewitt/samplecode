﻿using System;
using Common;

namespace Domain.Services.BotTemplates  
{
    public class CreateBotEmailTemplate
    {
        public Guid Id { get; set; }
        public Guid BotId { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public  MessageType MessageType { get; set; }
        public IMessageMetadata Metadata { get; set; }
    }
}

