﻿using System;
using Common;

namespace Domain.Services.BotTemplates
{
    public class BotEmailTemplateQueryRequest
    {
        public Guid Id { get; set; }
        public Guid BotId { get; set; }
        public string Body { get; set; }
        public string Subject { get; set; }
        public bool IsSystem { get; set; }
        public string Metadata { get; set; }
        public string MessageType { get; set; }
        public DateTime UpdatedAt { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
