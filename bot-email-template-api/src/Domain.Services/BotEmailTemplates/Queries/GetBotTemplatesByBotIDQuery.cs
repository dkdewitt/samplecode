﻿using System;
using Data.DataAccess;

namespace Domain.Services.BotTemplates.Queries
{
    public class GetBotTemplatesByBotIDQuery : IQuery<BotEmailTemplateQueryRequest>
    {
        public string Sql
        {
            get
            {
                return @"
                    SELECT
                        id,
                        bot_id,
                        subject,
                        body,
                        is_system,
                        metadata,
                        message_type,
                        created_at,
                        updated_at
                    FROM public.bot_email_templates
                    WHERE bot_id = @BotID
                ";
            }
        }

        public Guid BotID { get; set; }
        public object Parameters => this;
    }
}

