﻿using System;
namespace Data
{
    public sealed class ConnectionString: IConnectionString
    {
        public ConnectionString(string value) => Value = value;
        public string GetConnectionString() => Value;
        public string Value { get; }
    }

}

