﻿using System;
namespace Data.DataAccess
{
    public interface IQuery
    {
        string Sql { get; }

        object Parameters { get; }
    }

    public interface IQuery<out T>: IQuery { }
    public interface ExecuteNonQuery: IQuery<int> { }
}

