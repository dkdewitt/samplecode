﻿using System;
namespace Data.DataAccess
{
    public interface IDataContext
    {
        IEnumerable<T> Query<T>(IQuery<T> query, int? commandTimeout = null);
        IEnumerable<T> Query<T>(IQuery query, int? commandTimeout = null);
        Task<IEnumerable<T>> QueryAsync<T>(IQuery<T> query, int? commandTimeout = null);
        Task<int> ExecuteNonQueryAsync<T>(IQuery query, int? commandTimeout = null);
    }
}

