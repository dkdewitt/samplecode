﻿using System;
using System.Data;
using Dapper;
using Npgsql;
namespace Data.DataAccess
{
    
    public class PostgresDataContext : IDataContext
    {
        public readonly IConnectionString _connectionString;
        
        public PostgresDataContext(IConnectionString connectionString)
        {
            _connectionString = connectionString;
        }

        public IEnumerable<T> Query<T>(IQuery<T> query, int? commandTimeout = null) => this.ExecuteInternal(connection =>
        {
            return connection.Query<T>(query.Sql, query.Parameters, commandTimeout: commandTimeout);
                                                                                     
        }, query);

        public async Task<IEnumerable<T>> QueryAsync<T>(IQuery<T> query, int? commandTimeout = null)
        {
            return await this.ExecuteInternalAsync(async connection =>
            {
                return await connection.QueryAsync<T>(query.Sql, query.Parameters, commandTimeout: commandTimeout).ConfigureAwait(false);
            }, query).ConfigureAwait(false);
        }


        protected async Task<T> ExecuteInternalAsync<T>(Func<IDbConnection, Task<T>> action, IQuery query, int? commandTimeout = null)
        {
            Dapper.DefaultTypeMap.MatchNamesWithUnderscores = true;
            using (var connection = new NpgsqlConnection(_connectionString.GetConnectionString()))
            {
                return await action(connection).ConfigureAwait(false);
            }

        }

        protected T ExecuteInternal<T>(Func<IDbConnection, T> action, IQuery query, int? commandTimeout = null)
        {
                Dapper.DefaultTypeMap.MatchNamesWithUnderscores = true;
                using (var connection = new NpgsqlConnection(_connectionString.GetConnectionString()))
            {
                    connection.Open();
                    var res =  action(connection);
                    return res;
            }

        }

        public IEnumerable<T> Query<T>(IQuery query, int? commandTimeout = null)
        {
            Dapper.DefaultTypeMap.MatchNamesWithUnderscores = true;
            return this.ExecuteInternal(connection =>
            {
                var res = connection.Query<T>(query.Sql, query.Parameters);
                return res;
            }, query);

        }

        public async Task<int> ExecuteNonQueryAsync<T>(IQuery query, int? commandTimeout = null)
        {
            return await this.ExecuteInternalAsync<int>(async connection =>
            {
                return await connection.ExecuteAsync(query.Sql, query.Parameters).ConfigureAwait(false);

            }, query).ConfigureAwait(false);
        }
    }
    
}

