﻿using System;
namespace Data
{
    public interface IConnectionString
    {
        string GetConnectionString();
    }
}

