﻿using System;
using Api.Models;
using Api.Services.BotTemplates.Create;
using Domain.Services.BotTemplates;
using FluentValidation;
using Common;
namespace Api.Validators
{
    public class CreateBotEmailTemplateVMValidator : AbstractValidator<CreateBotEmailTemplateVM>
    {
        public CreateBotEmailTemplateVMValidator()
        {
            RuleFor(x => x.MessageType).NotEmpty();
            RuleFor(x => x.MessageType).IsInEnum().WithMessage("Please specify a first name");
            RuleFor(x => x).Must(x=> BeAValidMessageType(Enum.Parse<MessageType>(x.MessageType), x.Metadata)).WithMessage("Please specify a valid postcode");

        }
            private bool BeAValidMessageType(MessageType messageType, IMessageMetadata metadata )
            {
                // custom postcode validating logic goes here

                if(messageType == MessageType.FirstService)
                {
                    
                    FirstService firstService = (FirstService)metadata;

                    if(firstService.Step < 0)
                    {
                        throw new ValidationException("Negative step value not allowed");
                    }

                }
                return true;
            }
        
    }
}