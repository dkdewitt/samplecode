using System.Data;
using System.Net;
using Api.Models;
using Api.Services.BotTemplates;
using Api.Services.BotTemplates.Create;
using Api.Services.BotTemplates.Get;
using Common;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers;

[ApiController]
[Route("[controller]")]
public class EmailTemplateController : ControllerBase
{
    private readonly ILogger<EmailTemplateController> logger;
    private readonly ICreateBotEmailTemplateApiService createBotEmailTemplateService;
    private readonly IGetBotEmailTemplateApiService getBotEmailTemplateApiService;
    public EmailTemplateController(ILogger<EmailTemplateController> logger, ICreateBotEmailTemplateApiService createBotEmailTemplateService, IGetBotEmailTemplateApiService getBotEmailTemplateApiService)
    {
        this.logger = logger;
        this.createBotEmailTemplateService = createBotEmailTemplateService;
        this.getBotEmailTemplateApiService = getBotEmailTemplateApiService;
    }

    [HttpGet(Name = "GetBotEmailTemplates")]
    public async Task<IEnumerable<GetBotEmailTemplateResultDto>> GetAsync(Guid botID)
    {

        var result = await getBotEmailTemplateApiService.GetBotEmailTemplatesByBotID(botID);
        return result;
    }

    [HttpPost(Name = "PostBotEmailTemplate")]
    public async Task<CreateBotEmailTemplateResultDto> PostAsync(CreateBotEmailTemplateVM model)
    {
            var result = await createBotEmailTemplateService.Create(new CreateBotTemplateRequestDto
            {
                BotId = model.BotId,
                Body = model.Body,
                Subject = model.Subject,
                MessageType = Enum.Parse<MessageType>(model.MessageType),
                Metadata = model.Metadata
            });
            return result;
    }
}
