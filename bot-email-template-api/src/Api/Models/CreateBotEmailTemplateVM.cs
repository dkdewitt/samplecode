﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using Common;
using Newtonsoft.Json.Converters;

namespace Api.Models
{
    public class CreateBotEmailTemplateVM
    {
        [Required]
        public Guid BotId { get; set; }
        [Required]
        public string Subject { get; set; }
        [Required]
        public string Body { get; set; }
        [Required]
        public string MessageType { get; set; }
        //[JsonConverter(Services.Converters.MetadataConverter)]
        public IMessageMetadata Metadata { get; set; }


    }
}

