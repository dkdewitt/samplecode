using System.Data;
using System.Data.Common;
using System.Text.Json.Serialization;
using Api.Services.BotTemplates.Create;
using Api.Services.BotTemplates.Get;
using Data;
using Data.DataAccess;
using Domain.Services.BotEmailTemplates;
using Domain.Services.BotTemplates;
using Microsoft.AspNetCore.Http.Json;
using Npgsql;


var builder = WebApplication.CreateBuilder(args);
ConfigurationManager configuration = builder.Configuration;
// Add services to the container.
builder.Services.Configure<JsonOptions>(opt => opt.SerializerOptions.Converters.Add(new JsonStringEnumConverter()));

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
var connectionString = new ConnectionString(configuration.GetConnectionString("DefaultConnection"));

builder.Services.AddTransient<IConnectionString>((sp) => new ConnectionString(connectionString.GetConnectionString()));
builder.Services.AddScoped<ICreateBotEmailTemplateService, CreateBotEmailTemplateService>();
builder.Services.AddScoped<IGetBotEmailTemplateService, GetBotEmailTemplateService>();
builder.Services.AddScoped<IGetBotEmailTemplateApiService, GetBotEmailTemplateApiService>();
builder.Services.AddScoped<ICreateBotEmailTemplateApiService, CreateBotEmailTemplateApiService>();
builder.Services.AddTransient<IDataContext>(sp => new PostgresDataContext(connectionString));
var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
