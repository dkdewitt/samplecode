﻿using System;
using FluentMigrator;
namespace Migrations.V01
{
    [Migration(2021112100001)]
    public class AddBotEmailTemplate: Migration
    {
        public override void Up()
        {
            //Create.Table("bot_email_template")
            //    .WithColumn("Id").AsGuid().PrimaryKey()
            //    .WithColumn("Text").AsString();]

            Execute.Sql(@"
CREATE TABLE public.bot_email_templates
(
    id uuid NOT NULL,
    bot_id uuid,
    subject character varying,
    body character varying,
    is_system boolean,
    metadata jsonb,
    message_type character varying,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    CONSTRAINT bot_email_templates_pkey PRIMARY KEY(id)
)

TABLESPACE pg_default;

ALTER TABLE public.bot_email_templates
    OWNER to bot_templates;
            ");
        }

        public override void Down()
        {
            Delete.Table("public.bot_email_templates");
        }
    }
}

