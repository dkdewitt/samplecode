using FluentValidation.TestHelper;
using SampleCode.LoanCalculator;
using Xunit;

namespace SampleCodeTests;

public class LoanCalculatorRequestDtoValidatorTests
{
    [Fact]
    public void LoanCalculatorRequestDtoValidatorTests_Validate_Null_Test() 
    {
        var validator = new LoanCalculatorRequestDtoValidator();
        var model = new LoanCalculatorRequestDto { };
        var result = validator.TestValidate<LoanCalculatorRequestDto>(model);
        result.ShouldHaveValidationErrorFor(m => m.Amount);
        result.ShouldHaveValidationErrorFor(m => m.DownPayment);
        result.ShouldHaveValidationErrorFor(m => m.Interest);
        result.ShouldHaveValidationErrorFor(m => m.Term);
    }

    [Fact]
    public void LoanCalculatorRequestDtoValidatorTests_Validate_Amount_Less_Zero_Test()
    {
        var validator = new LoanCalculatorRequestDtoValidator();
        var model = new LoanCalculatorRequestDto { Amount = -1};
        var result = validator.TestValidate<LoanCalculatorRequestDto>(model);
        result.ShouldHaveValidationErrorFor(m => m.Amount).WithErrorCode(LoanCalculatorRequestDtoValidationCodes.AMOUNT_LESS_THAN_OR_EQUAL_TO_ZERO);
    }

    [Fact]
    public void LoanCalculatorRequestDtoValidatorTests_Validate_Interest_Less_Zero_Test()
    {
        var validator = new LoanCalculatorRequestDtoValidator();
        var model = new LoanCalculatorRequestDto { Amount = 2, Interest = -2 };
        var result = validator.TestValidate<LoanCalculatorRequestDto>(model);
        result.ShouldHaveValidationErrorFor(m => m.Interest).WithErrorCode(LoanCalculatorRequestDtoValidationCodes.INTEREST_LESS_THAN_OR_EQUAL_TO_ZERO);
    }

    [Fact]
    public void LoanCalculatorRequestDtoValidatorTests_Validate_DownPayment_Less_Zero_Test()
    {
        var validator = new LoanCalculatorRequestDtoValidator();
        var model = new LoanCalculatorRequestDto { Amount = 2, Interest = 2, DownPayment = -1 };
        var result = validator.TestValidate<LoanCalculatorRequestDto>(model);
        result.ShouldHaveValidationErrorFor(m => m.DownPayment).WithErrorCode(LoanCalculatorRequestDtoValidationCodes.DOWN_PAYMENT_LESS_THAN_OR_EQUAL_TO_ZERO);
    }

    [Fact]
    public void LoanCalculatorRequestDtoValidatorTests_Validate_DownPayment_Greater_Than_Amount_Test()
    {
        var validator = new LoanCalculatorRequestDtoValidator();
        var model = new LoanCalculatorRequestDto { Amount = 2, Interest = 2, DownPayment = 3 };
        var result = validator.TestValidate<LoanCalculatorRequestDto>(model);
        result.ShouldHaveValidationErrorFor(m => m.DownPayment).WithErrorCode(LoanCalculatorRequestDtoValidationCodes.DOWN_PAYMENT_GREATER_THAN_AMOUNT);
    }

    [Fact]
    public void LoanCalculatorRequestDtoValidatorTests_Validate_Term_Less_Zero_Test()
    {
        var validator = new LoanCalculatorRequestDtoValidator();
        var model = new LoanCalculatorRequestDto { Amount = 2, Interest = 2, DownPayment = 2, Term=-1 };
        var result = validator.TestValidate<LoanCalculatorRequestDto>(model);
        result.ShouldHaveValidationErrorFor(m => m.Term).WithErrorCode(LoanCalculatorRequestDtoValidationCodes.TERM_LESS_THAN_OR_EQUAL_TO_ZERO);
    }
}