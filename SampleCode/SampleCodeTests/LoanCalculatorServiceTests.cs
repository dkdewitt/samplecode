﻿using SampleCode.LoanCalculator;
using Xunit;
using Moq;
using FluentValidation;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using FluentValidation.Results;

namespace SampleCodeTests
{
	public class LoanCalculatorServiceTests
	{
		[Fact]
		public void LoanCalculatorServiceTests_Valid_Test()
		{
			var validaterMock = new Mock<IValidator<LoanCalculatorRequestDto>>();
			validaterMock.Setup(m => m.Validate(It.IsAny<LoanCalculatorRequestDto>())).Returns(new FluentValidation.Results.ValidationResult { });
			var loggerMock = new Mock<ILogger<LoanCalculatorService>>();
			var service = new LoanCalculatorService(validaterMock.Object, loggerMock.Object);

			var result = service.Calculate(new LoanCalculatorRequestDto
			{
				Amount = 100000,
				Interest = 5.5,
				DownPayment = 20000,
				Term = 30

			});

			Assert.Equal<decimal>(new Decimal(454.23), result.MonthlyPayment);
			Assert.Equal<decimal>(new Decimal(83522.80), result.TotalInterest);
			Assert.Equal<decimal>(new Decimal(163522.80), result.TotalPayment);
		}

		[Fact]
		public void LoanCalculatorServiceTests_InValid_Throws_Test()
		{
			var validaterMock = new Mock<IValidator<LoanCalculatorRequestDto>>();

			var validationResult = new ValidationResult();
			validationResult.Errors.Add(new ValidationFailure("SomeProperty", "SomeError"));

			validaterMock.Setup(m => m.Validate(It.IsAny<LoanCalculatorRequestDto>())).Returns(
				validationResult
				);
			var loggerMock = new Mock<ILogger<LoanCalculatorService>>();
			var service = new LoanCalculatorService(validaterMock.Object, loggerMock.Object);


			Assert.Throws<ValidationException>(() => service.Calculate(new LoanCalculatorRequestDto
			{
				Amount = 100000,
				Interest = 5.5,
				DownPayment = 20000,
				Term = 30

			}));
		}
	}
}
