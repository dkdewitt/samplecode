# Welcome to SampleCode
## The most advanced mortgage calculator in the world

### System Requirements
* Install dotnet 6

### Libraries used
* FluentValidator for validation
* XUnit and Moq for unit testing
* Microsoft default logger

### To Run the Project
* Navigate to project
* run `dotnet restore`
* run `dotnet build`
*  run `dotnet test` to run unit tests
*  run `dotnet run --project SampleCode/SampleCode.csproj` to run app

### Documentation
When running the app the Console will prompt users to input data.  It is expected in the following format:
```
amount: 100000
interest: 5.5%
downpayment: 20000
term: 30

```

The calculator will validate data and if valid will calculate "monthly payment", "total interest" and "total payment"
The result will be a JSON object logged in output.
Invalid inputs will fail.