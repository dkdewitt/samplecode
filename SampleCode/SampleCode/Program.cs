﻿using FluentValidation;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using SampleCode.LoanCalculator;
using Microsoft.Extensions.Logging;
using System.Text.Json;

using IHost host = Host.CreateDefaultBuilder(args)
    .ConfigureServices((_, services) =>
        services
            .AddScoped<ILoanCalculatorService, LoanCalculatorService>()
            .AddScoped<IValidator<LoanCalculatorRequestDto>, LoanCalculatorRequestDtoValidator>())
    .ConfigureLogging(logging =>
    {
        logging.ClearProviders();
        logging.AddConsole();
    })
    .Build();

var logger = host.Services.GetRequiredService<ILogger<Program>>();
try
{
    Console.WriteLine("Welcome to SampleCode. Please enter data in the following format and order: \namount: <AMOUNT>\ninterest: <RATE>\ndownpayment: <AMOUNT>\nterm: <LENGTH IN YEARS>\n\nEnter data:\n");
    var request = ParseInput();
    var result = JsonSerializer.Serialize<LoanCalculatorResponseDto>(RunCalc(host.Services, request));
    logger.LogInformation(result);
} catch(Exception ex)
{
    logger.LogError(ex.Message);
}

static LoanCalculatorResponseDto RunCalc(IServiceProvider services, LoanCalculatorRequestDto request)
{
    using IServiceScope serviceScope = services.CreateScope();
    IServiceProvider provider = serviceScope.ServiceProvider;
    var service = provider.GetRequiredService<ILoanCalculatorService>();
    return service.Calculate(request);
}

static LoanCalculatorRequestDto ParseInput()
{
    string line;
    int i = 0;
    string delimiter = ": ";
    List<string> lines = new List<string>();
    while (!string.IsNullOrWhiteSpace(value: line = Console.ReadLine()))
    {
        if(i > 4)
        {
            throw new ArgumentOutOfRangeException("Input is invalid");
        }

        lines.Add(line);

        i++;
    }

    var amountResult = decimal.TryParse(lines[0].Split(delimiter)[1], out decimal amount);
    var interestResult = double.TryParse(lines[1].Split(delimiter)[1].Replace("%", ""), out double interest);
    var downPaymentResult = decimal.TryParse( lines[2].Split(delimiter)[1], out decimal downPayment);
    var termResult = int.TryParse( lines[3].Split(delimiter)[1], out int term);
    var errors = new List<string>();
    if (!amountResult) errors.Add("Invalid amount provided. Value must be greater than 0.");
    if (!interestResult) errors.Add("Invalid interest provided. Value must be greater than 0.");
    if (!downPaymentResult) errors.Add("Invalid downpayment provided. Value must be greater than 0.");
    if (!termResult) errors.Add("Invalid term provided. Value must be greater than 0.");

    if (errors.Count > 0)
        throw new ArgumentException(String.Join("Invalid parameters provided: ", errors.Select(e => $"\n{e}")));

    return new LoanCalculatorRequestDto
    {
        Amount = amount,
        Interest = interest,
        DownPayment = downPayment,
        Term = term
    };
}