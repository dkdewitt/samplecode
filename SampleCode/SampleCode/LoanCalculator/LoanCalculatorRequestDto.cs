﻿using System;
using FluentValidation;
namespace SampleCode.LoanCalculator
{
	public class LoanCalculatorRequestDto
	{
		public decimal Amount { get; set; }
		public double Interest { get; set; }
		public decimal DownPayment { get; set; }
		public int Term { get; set; }
	}

	public static class LoanCalculatorRequestDtoValidationCodes
	{
		public const string AMOUNT_LESS_THAN_OR_EQUAL_TO_ZERO = nameof(AMOUNT_LESS_THAN_OR_EQUAL_TO_ZERO);
		public const string INTEREST_LESS_THAN_OR_EQUAL_TO_ZERO = nameof(INTEREST_LESS_THAN_OR_EQUAL_TO_ZERO);
		public const string DOWN_PAYMENT_LESS_THAN_OR_EQUAL_TO_ZERO = nameof(DOWN_PAYMENT_LESS_THAN_OR_EQUAL_TO_ZERO);
		public const string DOWN_PAYMENT_GREATER_THAN_AMOUNT = nameof(DOWN_PAYMENT_GREATER_THAN_AMOUNT);
		public const string TERM_LESS_THAN_OR_EQUAL_TO_ZERO = nameof(TERM_LESS_THAN_OR_EQUAL_TO_ZERO);
	}

	public class LoanCalculatorRequestDtoValidator : AbstractValidator<LoanCalculatorRequestDto>
	{
		public LoanCalculatorRequestDtoValidator()
		{
			RuleFor(x => x.Amount)
				.NotNull()
				.NotEmpty()
				.GreaterThan(0).WithErrorCode(LoanCalculatorRequestDtoValidationCodes.AMOUNT_LESS_THAN_OR_EQUAL_TO_ZERO);
			RuleFor(x => x.Interest)
				.NotNull()
				.NotEmpty()
				.GreaterThan(0).WithErrorCode(LoanCalculatorRequestDtoValidationCodes.INTEREST_LESS_THAN_OR_EQUAL_TO_ZERO);
			RuleFor(x => x.DownPayment)
				.NotNull()
				.NotEmpty()
				.GreaterThanOrEqualTo(0).WithErrorCode("DOWN_PAYMENT_LESS_THAN_OR_EQUAL_TO_ZERO")
				.LessThanOrEqualTo(x=>x.Amount).WithErrorCode("DOWN_PAYMENT_GREATER_THAN_AMOUNT");
			RuleFor(x => x.Term)
				.NotNull()
				.NotEmpty()
				.GreaterThan(0).WithErrorCode("TERM_LESS_THAN_OR_EQUAL_TO_ZERO");
		}
	}

}

