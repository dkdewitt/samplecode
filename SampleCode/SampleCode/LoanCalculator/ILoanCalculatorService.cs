﻿using System;
namespace SampleCode.LoanCalculator
{
	public interface ILoanCalculatorService
	{
        LoanCalculatorResponseDto Calculate(LoanCalculatorRequestDto request);
	}
}

