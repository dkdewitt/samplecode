﻿using System;
using System.Text.Json;
using FluentValidation;
using Microsoft.Extensions.Logging;

namespace SampleCode.LoanCalculator
{
    public class LoanCalculatorService : ILoanCalculatorService
    {
        private readonly IValidator<LoanCalculatorRequestDto> loanCalculatorRequestDtoValidator;
        private readonly ILogger<LoanCalculatorService> logger;
        public LoanCalculatorService(IValidator<LoanCalculatorRequestDto> loanCalculatorRequestDtoValidator, ILogger<LoanCalculatorService> logger)
        {
            this.loanCalculatorRequestDtoValidator = loanCalculatorRequestDtoValidator;
            this.logger = logger;
        }
        public LoanCalculatorResponseDto Calculate(LoanCalculatorRequestDto request)
        {
            logger.LogDebug("Calculating Loan Information for request: ", JsonSerializer.Serialize(request));
            var validation = this.loanCalculatorRequestDtoValidator.Validate(request);
            if(!(validation.IsValid))
            {
                logger.LogError("Request has validation errors: {messages}", validation.Errors.Select(v => v.ErrorMessage));
                throw new ValidationException(validation.Errors);
            }
            var interest = ((request.Interest / 100) / 12);
            var downPayment = request.DownPayment;
            var term = request.Term * 12;
            var amount = request.Amount;

            var loanAmount = (amount - downPayment);

            decimal monthlyPayment = Math.Round(loanAmount * ((decimal)(interest * Math.Pow((1 + interest), term)) / (decimal)(Math.Pow(1 + interest, term) - 1)), 2);

            var totalAmount = monthlyPayment * (term);

            return new LoanCalculatorResponseDto
            {
                MonthlyPayment = monthlyPayment,
                TotalInterest = (totalAmount - loanAmount),
                TotalPayment = totalAmount
            };
        }
    }
}

