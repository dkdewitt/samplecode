﻿using System;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace SampleCode.LoanCalculator
{
	public class LoanCalculatorResponseDto
	{
		[JsonPropertyName("monthly payment")]
		public decimal MonthlyPayment { get; set; }
		[JsonPropertyName("total interest")]
		public decimal TotalInterest { get; set; }
		[JsonPropertyName("total payment")]
		public decimal TotalPayment { get; set; }
	}
}

